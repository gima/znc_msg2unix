# msg2unix

ZNC module that writes incoming messages on #channels to UNIX domain socket as JSON.

<sup>(The module can be loaded globally, per-user and per-network.)</sup>

## Usage

1. Copy the module to a folder [where ZNC looks for modules](http://wiki.znc.in/Modules#Managing_Modules).

2. Load the module. Can be done using web interface provided by [webadmin](http://wiki.znc.in/Webadmin) module or by private messaging to [controlpanel](http://wiki.znc.in/Controlpanel) module.

   In either case, the UNIX domain socket path is given as the module's argument.


## License

[MIT](LICENSE.txt)

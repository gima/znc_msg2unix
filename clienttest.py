import socket
import json

def write(bytes, path):
  with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as sock:
    sock.settimeout(1) # seconds
    try:
      sock.connect(path)
      sock.sendall(bytes)
      return True
    except OSError as e:
      raise RuntimeError("socket(unix:'{}'): {}".format(path, e)) from e
      
write(json.dumps({
  'ts': 123,
  'chan': '#chan',
  'msg': 'abc',
}).encode(), 'testsock.unix')

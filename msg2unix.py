# Homepage and license: https://github.com/gima/znc_msg2unix
# ZNC module that writes incoming messages on #channels to UNIX domain socket as JSON.

import os
import stat
import sys
import znc
import json
import socket
from datetime import datetime, timezone

def write(bytes, path):
  with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as sock:
    sock.settimeout(1) # seconds
    try:
      sock.connect(path)
      sock.sendall(bytes)
      return True
    except OSError as e:
      raise RuntimeError("socket(unix:'{}'): {}".format(path, e)) from e

class msg2unix(znc.Module):
  description = "Write incoming messages on #channels to UNIX domain socket (argument) as JSON. Homepage: https://github.com/gima/znc_msg2unix"
  # wiki_page = '' # webadmin's html template has hardcoded url prefix (to znc wiki)
  module_types = [znc.CModInfo.UserModule, znc.CModInfo.NetworkModule, znc.CModInfo.GlobalModule]
  has_args = True

  def OnLoad(self, arg, retmsg):
    if not arg:
      retmsg.s = "Argument must be path to listening unix socket"
      return False

    try:
      if not stat.S_ISSOCK(os.stat(arg).st_mode):
        retmsg.s = "Argument is not path to unix socket: '{}')".format(arg)
        return False
    except OSError as e:
      retmsg.s = "stat('{}'): {}".format(arg, e)
      return False

    self.__path = arg
    return True

  def OnChanMsg(self, nick, chan, msg):
    j = json.dumps({
      'ts': datetime.now(timezone.utc).isoformat(),
      'chan': chan.GetName(),
      'server': {
        'name': self.GetNetwork().GetName(),
        'address': self.GetNetwork().GetCurrentServer().GetString(),
      },
      "user": {
        'perm': nick.GetPermStr(),
        'nick': nick.GetNick(),
        'ident': nick.GetIdent(),
        'host': nick.GetHost(),
      },
      'msg': msg.s,
    })
    try:
      write(j.encode(), self.__path)
    except:
      self.PutModule('Error! Could not write message to unix socket. {}'.format(sys.exc_info()[1]))

    return znc.CONTINUE
